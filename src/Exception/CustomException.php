<?php

namespace DataCube\DataCubeAggregation\Exception;

use Throwable;

/**
 * wrap
 */
final class CustomException extends \Exception
{
    protected $detailMessage;

    public function __construct($message, $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
        $this->detailMessage = $message;
    }
}
