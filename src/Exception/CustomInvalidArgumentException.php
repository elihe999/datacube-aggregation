<?php

namespace DataCube\DataCubeAggregation\Exception;

use Throwable;

/**
 * wrap
 */
final class CustomInvalidArgumentException extends \InvalidArgumentException implements ExceptionInterface
{
    protected $detailMessage;  
  
    public function __construct($message, $code = 0, Throwable $previous = null) {  
        parent::__construct($message, $code, $previous);  
        $this->detailMessage = $message;  
    }  

    public static function getWrongType($argumentName, $value, $expectedType)
    {
        return new self(
            sprintf(
                'The parameter "%s" expect to be of type %s, %s given.',
                $argumentName,
                $expectedType,
                is_object($value) ? get_class($value) : gettype($value)
            )
        );
    }
}