<?php

namespace DataCube\DataCubeAggregation\Utils\Graphics;

class Pivot
{
    // 行配置
    public $rowSettings = null;
    // 列配置
    public $columnSettings = null;
    // 值配置
    public $valueSettings = null;
    // 过滤配置
    public $filterSettings = null;

    public $tableTitle = 'new pivot';
    public $data = null;

    public $rowAggregate = null;
    public $columnAggregate = null;
    // public $sumColumnAggregate = null;

    public $aggregateLists = [
        'count',
        'average',
        'sum',
        'max',
        'min'
    ];

    function __construct(array $conf = [])
    {
        if (!empty($conf)) {
            $rowsConfig = !empty($conf['rows']) && json_decode($conf['rows']) ? json_decode($conf['rows'], true) : null;
            $columnsConfig = !empty($conf['columns']) && json_decode($conf['columns']) ? json_decode($conf['columns'], true) : null;
            $valuesConfig = !empty($conf['values']) && json_decode($conf['values']) ? json_decode($conf['values'], true) : null;
            $this->data = !empty($conf['data']) ? $conf['data'] : null;
        }
    }

    // 透视表函数  
    public function pivotTable($data, $rows, $cols)
    {
        $pivotTable = array();

        // 构建透视表  
        foreach ($data as $row) {
            $rowData = array_combine($rows, array_slice($row, 0, count($rows)));
            $pivotRow = &$pivotTable[implode('-', $rowData)];

            foreach ($cols as $i => $col) {
                $colData = $row[$i + count($rows)];
                $pivotRow[$col] = $colData;
            }
        }

        return $pivotTable;
    }

    public function getTableTitle()
    {
        return $this->tableTitle;
    }

    public function setTableTitle(string $name): void
    {
        $this->tableTitle = $name;
    }

    /**
     * copy from: https://packagist.org/packages/jakezatecky/array_group_by
     *
     * @param array $arr
     * @param $key
     * @return array
     */
    function array_group_by(array $arr, $key): array
    {
        if (!is_string($key) && !is_int($key) && !is_float($key) && !is_callable($key)) {
            trigger_error('array_group_by(): The key should be a string, an integer, a float, or a function', E_USER_ERROR);
        }

        $isFunction = !is_string($key) && is_callable($key);

        // Load the new array, splitting by the target key
        $grouped = [];
        foreach ($arr as $value) {
            $groupKey = null;

            if ($isFunction) {
                $groupKey = $key($value);
            } else if (is_object($value)) {
                $groupKey = $value->{$key};
            } else {
                $groupKey = $value[$key];
            }

            $grouped[$groupKey][] = $value;
        }

        // Recursively build a nested grouping if more parameters are supplied
        // Each grouped array value is grouped according to the next sequential key
        if (func_num_args() > 2) {
            $args = func_get_args();

            foreach ($grouped as $groupKey => $value) {
                $params = array_merge([$value], array_slice($args, 2, func_num_args()));
                // $grouped[$groupKey] = call_user_func_array('array_group_by', $params);
                $grouped[$groupKey] = $this->array_group_by(...$params);
            }
        }

        return $grouped;
    }
}
