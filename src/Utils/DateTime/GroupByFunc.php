<?php

namespace DataCube\DataCubeAggregation\Utils\DateTime;

class GroupByFunc implements GroupByFunctionInterface
{
    // 按月份分组函数
    public static function groupTimestampsByMonth(array $timestamps): array
    {
        $groupByMonth = [];

        foreach ($timestamps as $timestamp) {
            $date = new \DateTime();
            $date->setTimestamp($timestamp);
            $month = strval($date->format('m'));

            if (!isset($groupByMonth[$month])) {
                $groupByMonth[$month] = [];
            }

            $groupByMonth[$month][] = $timestamp;
        }

        return $groupByMonth;
    }

    // 按季度分组函数
    public static function groupTimestampsByQuarter($timestamps): array
    {
        $groupByQuarter = [];

        foreach ($timestamps as $timestamp) {
            $date = new \DateTime();
            $date->setTimestamp($timestamp);
            $month = $date->format('m');
            $quarter = ceil($month / 3);

            if (!isset($groupByQuarter[$quarter])) {
                $groupByQuarter[$quarter] = [];
            }

            $groupByQuarter[$quarter][] = $timestamp;
        }

        return $groupByQuarter;
    }

    public static function groupTimestampsByYear(array $timestamps): array
    {
        return [];
    }

    public static function groupTimestampsByWeek(array $timestamps): array
    {
        return [];
    }

    public static function groupTimestampsByHour(array $timestamps): array
    {
        return [];
    }

    public static function groupTimestampsByMinutes(array $timestamps): array
    {
        return [];
    }

    public static function groupTimestampsBySecond(array $timestamps): array
    {
        return [];
    }

    // 按天分组时间戳
    public static function groupTimestampsByDay(array $timestamps): array
    {
        $groupByDay = [];

        foreach ($timestamps as $timestamp) {
            $date = new \DateTime();
            $date->setTimestamp($timestamp);
            $day = $date->format('z'); // 使用 'z' 格式获取年份中的第几天

            if (!isset($groupByDay[$day])) {
                $groupByDay[$day] = [];
            }

            $groupByDay[$day][] = $timestamp;
        }

        return $groupByDay;
    }

    // 判断月份上中下旬
    public static function groupTimestampsByEMBMonth(array $timestamps): array
    {
        // default setting
        $leapFeb = [10, 10, 8];
        $normalFeb = [10, 10, 9];
        $solarMonth = [10, 10, 11];
        $lunarMonth = [10, 10, 10];

        $groupByMonthPhase = [];
        foreach ($timestamps as $timestamp) {
            $date = new \DateTime();
            $date->setTimestamp($timestamp);
            $day = $date->format('j');
            $month = $date->format('n');
            $isLeapYear = $date->format('L');
            $isBigMonth = in_array(intval($month), [1, 3, 5, 7, 8, 10, 12]);
            if ($isLeapYear && $month == 2) {
                $dayData = $leapFeb;
            } elseif ($month == 2) {
                $dayData = $normalFeb;
            } elseif ($isBigMonth) {
                $dayData = $solarMonth;
            } else {
                $dayData = $lunarMonth;
            }
            if ($day <= $dayData[0]) {
                $monthPhase = '上旬';
            } elseif ($day <= $dayData[1]) {
                $monthPhase = '中旬';
            } else {
                $monthPhase = '下旬';
            }
            if (!isset($groupByMonthPhase[$monthPhase])) {
                $groupByMonthPhase[$monthPhase] = [];
            }

            $groupByMonthPhase[$monthPhase][] = $timestamp;
        }

        return $groupByMonthPhase;
    }

    public static function groupTimestampsByAMPM(array $timestamps): array
    {
        return [];
    }
}
