<?php

namespace DataCube\DataCubeAggregation\Utils\DateTime;

interface GroupByFunctionInterface
{
    public static function groupTimestampsByQuarter(array $timestamps): array;
    public static function groupTimestampsByMonth(array $timestamps): array;
    public static function groupTimestampsByYear(array $timestamps): array;
    public static function groupTimestampsByWeek(array $timestamps): array;
    public static function groupTimestampsByHour(array $timestamps): array;
    public static function groupTimestampsByMinutes(array $timestamps): array;
    public static function groupTimestampsBySecond(array $timestamps): array;

    // Group By Day
    public static function groupTimestampsByDay(array $timestamps): array;

    // B/M/E
    public static function groupTimestampsByEMBMonth(array $timestamps): array;
    public static function groupTimestampsByAMPM(array $timestamps): array;
}