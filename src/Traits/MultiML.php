<?php

namespace DataCube\DataCubeAggregation\Traits;

trait MultiML
{
    public function mlPackagistNameList(): array
    {
        return array(
            'rubixml',
            'phpml'
        );
    }
}
