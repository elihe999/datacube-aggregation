<?php

namespace DataCube\DataCubeAggregation\Functions\Aggregate;

class Summarize
{
    public function countOfRows(array $data, $column)
    {

    }

    public function sumOf($data, $column)
    {

    }

    public function AverageOf($data, $column)
    {

    }

    public function NumberOfDistinctRows(array $data, $column)
    {

    }

    /**
     * add preview value to sum of
     */
    public function cumulativeSumOf(array $data, $column)
    {

    }
}