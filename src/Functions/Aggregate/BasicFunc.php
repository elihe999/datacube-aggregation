<?php

namespace DataCube\DataCubeAggregation\Functions\Aggregate;

class BasicFunc
{
    public function sum(array $data)
    {
        return array_sum($data);
    }

    public function avg()
    {
    }

    public function count(array $data, $distinct = null)
    {
        if (!empty($distinct)) {
            if ($distinct === true) {
                return count(array_unique($data));
            } else {
                // unique_multidim_array
                return count($this->unique_multidim_array($data, $distinct));
            }
        }
        return count($data);
    }

    function unique_multidim_array($array, $key): array
    {
        $uniq_array = array();
        $dup_array = array();
        $key_array = array();

        foreach ($array as $val) {
            if (!in_array($val[$key], $key_array)) {
                $key_array[] = $val[$key];
                $uniq_array[] = $val;
                /*
                # 1st list to check:
                # echo "ID or sth: " . $val['building_id'] . "; Something else: " . $val['nodes_name'] . (...) "\n";
                */
            } else {
                $dup_array[] = $val;
                /*
                # 2nd list to check:
                # echo "ID or sth: " . $val['building_id'] . "; Something else: " . $val['nodes_name'] . (...) "\n";
                */
            }
        }
        return array($uniq_array, $dup_array, /* $key_array */);
    }

    public function max()
    {
    }

    public function min()
    {
    }

    public function countRow()
    {

    }


}
