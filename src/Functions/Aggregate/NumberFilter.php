<?php

namespace DataCube\DataCubeAggregation\Functions\Aggregate;

class NumberFilter
{
    public function equalTo($data, $column, $expected)
    {

    }

    public function notEqualTo($data, $column, $expected)
    {

    }

    public function greaterThan($data, $column, $target)
    {

    }

    public function lessThan($data, $column, $target)
    {

    }

    public function greaterThanOrEqualTo($data, $column, $target)
    {

    }

    public function lessThanOrEqualTo($data, $column, $target)
    {

    }

    public function between($data, $column, $small, $big)
    {

    }

    public function isEmpty($data, $column, $isEmpty = false)
    {

    }

    public function isNotEmpty($data, $column)
    {

    }
}