<?php

namespace DataCube\DataCubeAggregation\Functions\Aggregate;

class TextFilter
{
    public function is($data, $column, $value)
    {
    }

    public function isNot($data, $column, $value)
    {
    }

    public function contains($data, $column, $value)
    {

    }

    public function doesNotContain($data, $column, $value)
    {}

    public function isEmpty($data, $column, $isEmpty = true)
    {}

    public function isNotEmpty($data, $column)
    {}

    public function startWith($data, $column, $value)
    {

    }

    public function endWith($data, $column, $value)
    {}
}