<?php

namespace DataCube\DataCubeAggregation\Functions\Aggregate;

class DateFilter
{
    public function today($data, $column)
    {
    }

    public function yesterday($data, $column)
    {
    }

    public function lastWeek($data, $column)
    {
    }

    public function lastSevenDays($data, $column)
    {}

    public function lastYear($data, $column)
    {

    }

    public function dateBetween($data, $column, $start, $end)
    {

    }

    public function dateOn($data, $column, $dateFormat)
    {

    }

    public function datePast($data, $column, $date)
    {

    }


    public function dateNext($data, $column, $date)
    {

    }

    public function dateBefore($data, $column, $date)
    {}

    public function dateAfter($data, $column, $date)
    {}
}