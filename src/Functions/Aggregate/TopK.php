<?php
namespace DataCube\DataCubeAggregation\Functions\Aggregate;

class TopK
{
    public static function topKMin(array $arr, $k)
    {
        $minHeap = new \SplMinHeap();
        foreach ($arr as $item) {
            $minHeap->insert($item);
        }
        $res = [];
        while (count($res) < $k) {
            $res[] = $minHeap->extract();
        }
        return $res;
    }

    public static function topKMax(array $arr, $k)
    {
        $maxHeap = new \SplMaxHeap();
        foreach ($arr as $item) {
            $maxHeap->insert($item);
        }
        $res = [];
        while (count($res) < $k) {
            $res[] = $maxHeap->extract();
        }
        return $res;
    }
}