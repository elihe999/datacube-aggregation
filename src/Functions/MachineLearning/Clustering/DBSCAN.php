<?php

namespace DataCube\DataCubeAggregation\Functions\MachineLearning\Clustering;

use Phpml\Math\Distance\Euclidean;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Phpml\Clustering\DBSCAN as PhpmlDBSCAN;

class DBSCAN
{
    public PhpmlDBSCAN $dbscan;
    public array $options;


    public function __construct(array $options = [])
    {
        $resolver = new OptionsResolver();
        $resolver->setDefaults([
            'epslion' => 2,
            'minSamples' => 3,
            'distance' => new Euclidean()
        ]);
        $this->options = $resolver->resolve($options);

        $this->dbscan = new PhpmlDBSCAN($this->options['epslion'], $this->options['minSamples']);
    }

    public function cluster(array $samples)
    {
        return $this->dbscan->cluster($samples);
    }
}
