<?php

namespace DataCube\DataCubeAggregation\Functions\MachineLearning\Clustering;

use Rubix\ML\Clusterers\Seeders\KMC2;
use Rubix\ML\Clusterers\Seeders\PlusPlus;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Rubix\ML\Clusterers\GaussianMixture as RubixGaussianMixture;

class GaussianMixture
{
    public RubixGaussianMixture $gmm;
    public array $options;

    public function __construct(array $options = [])
    {
        $resolver = new OptionsResolver();
        $this->configureOptions($resolver);
        
        $this->options = $resolver->resolve($options);

        $this->gmm = new RubixGaussianMixture(
            $this->options['estimator'], 
            $this->options['smoothing'], 
            $this->options['epochs'], 
            $this->options['minChange'], 
            $this->options['seeder']
        );
    }
    
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'estimator' => 5,
            'smoothing' => 1e-9,
            'epochs' => 100,
            'minChange' => 1e-3,
            'seeder' => new PlusPlus()
        ]);
    }

    public function predict($dataset)
    {
        return $this->gmm->predict($dataset);
    }

    public function train($dataset): void
    {
        $this->gmm->train($dataset);
    }
}