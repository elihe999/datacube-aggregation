<?php
namespace DataCube\DataCubeAggregation\Functions\MachineLearning\Clustering;
use Rubix\ML\Clusterers\DBSCAN as RubixDBSCAN;
use Rubix\ML\Datasets\Unlabeled;
use Symfony\Component\OptionsResolver\OptionsResolver;
class Rubix_DBSCAN
{
    public RubixDBSCAN $dbscan;
    public array $options;

    public function __construct(array $options = [])
    {
        $resolver = new OptionsResolver();
        $resolver->setDefaults([
            'radius' => 2,
            'minDensity' => 3,
            'tree' => null
        ]);
        $this->options = $resolver->resolve($options);
        // float $radius = 0.5, int $minDensity = 5, ?Spatial $tree = null
        $this->dbscan = new RubixDBSCAN($this->options['radius'], $this->options['minDensity'], $this->options['tree']);
    }

    public function cluster(array $samples)
    {
        return $this->dbscan->predict(new Unlabeled([$samples]));
    }
}