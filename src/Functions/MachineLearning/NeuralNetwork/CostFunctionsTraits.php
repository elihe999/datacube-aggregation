<?php

namespace DataCube\DataCubeAggregation\Functions\MachineLearning\NeuralNetwork;

use DataCube\DataCubeAggregation\Exception\CustomException;

trait CostFunctionsTraits
{
    public function rubixCostFunctionArrConvertor(array $customOptimizer, $disableDefaultThrows = true)
    {
        switch ($customOptimizer[0]) {
            case 'HuberLoss':
                $alpha = 0.9;
                return new \Rubix\ML\NeuralNet\CostFunctions\HuberLoss($alpha);
            case 'LeastSquares':
                return new \Rubix\ML\NeuralNet\CostFunctions\LeastSquares();
            case 'RelativeEntropy':
                return new \Rubix\ML\NeuralNet\CostFunctions\RelativeEntropy();
            case 'CrossEntropy':
                return new \Rubix\ML\NeuralNet\CostFunctions\CrossEntropy();
            default:
                if (!$disableDefaultThrows) {
                    throw new CustomException('Can not find optimizer');
                }
                return new \Rubix\ML\NeuralNet\CostFunctions\CrossEntropy();
        }
    }
}