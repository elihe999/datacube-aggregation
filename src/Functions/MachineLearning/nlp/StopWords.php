<?php
namespace DataCube\DataCubeAggregation\Functions\MachineLearning\nlp;

use InvalidArgumentException;
use Phpml\FeatureExtraction\StopWords as PhpmlStopWords;

class StopWords extends PhpmlStopWords
{
    public static function factory(string $language = 'English'): PhpmlStopWords
    {
        if ($language == 'Chinese' or $language == 'ChineseSimplified') {
            $className = __NAMESPACE__."\\StopWords\\ChineseSimplified";

            if (!class_exists($className)) {
                throw new InvalidArgumentException(sprintf('Can\'t find "%s" language for StopWords', $language));
            }

            return new $className();
        }
        return parent::factory($language);
    }
}