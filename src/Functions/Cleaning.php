<?php
namespace DataCube\DataCubeAggregation\Functions;

use DataCube\DataCubeAggregation\Exception\MethodNotFoundException;

class Cleaning
{
    public function __call($name, $arguments)
    {
        $realClass = __NAMESPACE__ . '\\Cleaning\\' . $name;
        if (class_exists($realClass)) {
            return new $realClass($arguments);
        }
        throw new MethodNotFoundException('Cannot found this cleaning function: ' . $name);
    }
}
