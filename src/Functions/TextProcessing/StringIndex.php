<?php
namespace DataCube\DataCubeAggregation\Functions\TextProcessing;

/**
 * 一种类型转换算子，它将指定的属性的值映射成数值型索引，使得只能对数值型数据做处理的算子 也可以对属性进行处理。该算子一般用于数据预处理，另外，不适合对于包含连续型数据的列执行该算 子，如ID列。
 *
 * 索引顺序表示字符串索引依照的规则，分为frequencyDesc、frequencyAsc 、alphabetDesc和alphabetAsc四种。
 */
class StringIndex
{
    public function stringIndex($inputArray) {
        array_multisort($inputArray, SORT_ASC, SORT_STRING);
        $indexMap = []; // 用于存储字符串到数值索引的映射
        $indexedArray = []; // 用于存储数值索引的数组

        foreach ($inputArray as $stringValue) {
            if (!isset($indexMap[$stringValue])) {
                // 如果当前字符串值还没有映射到数值索引，则创建一个新的索引
                $indexMap[$stringValue] = count($indexMap);
            }
            $indexedArray[] = $indexMap[$stringValue]; // 将数值索引添加到数组中
        }

        return $indexedArray;
    }

    public function sortByFrequency($inputArray) {
        // 统计每个数值的出现次数
        $frequencyMap = [];
        foreach ($inputArray as $value) {
            if (!isset($frequencyMap[$value])) {
                $frequencyMap[$value] = 0;
            }
            $frequencyMap[$value]++;
        }

        // 根据出现次数对数值进行排序
        uasort($frequencyMap, function ($a, $b) {
            return $b - $a; // 按照出现次数降序排序
        });

        // 根据排序后的频率映射重建原始数组
        $sortedArray = [];
        foreach ($frequencyMap as $value => $frequency) {
            for ($i = 0; $i < $frequency; $i++) {
                $sortedArray[] = $value;
            }
        }

        return $sortedArray;
    }
}
