<?php

namespace DataCube\DataCubeAggregation\Functions\PathFinding;

interface DomainLogicInterface
{
    public function getAdjacentNodes($node);

    public function calculateRealCost($node, $adjacent);

    public function calculateEstimatedCost($fromNode, $toNode);
}