<?php

namespace DataCube\DataCubeAggregation\Functions\PathFinding\AStar;

interface NodeIdentifierInterface
{
    public function getUniqueNodeId(): string;
}