<?php
namespace DataCube\DataCubeAggregation\Functions\PathFinding\AStar;

use DataCube\DataCubeAggregation\Functions\Math\AStar\TState;

class Node
{
    public $name;
    public $visited = false;
    public $adjacent_nodes = [];
    public $predecessor = null;
    public $min_distance = -1;
    public $_f;
    public $_g;
    public $_h;
    public $_p;
    public $disable = false;

    public $positions = [];

    public function setPosition(...$arg)
    {
        $this->positions = $arg;
    }

    /** @var TState */
    private $state;
    private string $id;
    /** @var Node<TState> | null  */
    private ?Node $parent = null;
    /** @psalm-suppress PropertyNotSetInConstructor Reading G should fail visibly if it hasn't been previously set */
    private  $gScore;
    /** @psalm-suppress PropertyNotSetInConstructor Reading H should fail visibly if it hasn't been previously set */
    private  $hScore;

    public function __construct($state)
    {
        $this->state = $state;
        $this->id = $state instanceof NodeIdentifierInterface ? $state->getUniqueNodeId() : serialize($state);
    }

    public function getState()
    {
        return $this->state;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function setParent(Node $parent): void
    {
        $this->parent = $parent;
    }

    public function getParent(): ?Node
    {
        return $this->parent;
    }

    public function getF()
    {
        return $this->getG() + $this->getH();
    }

    public function setG($score): void
    {
        $this->gScore = $score;
    }

    public function getG()
    {
        return $this->gScore;
    }

    public function setH($score): void
    {
        $this->hScore = $score;
    }

    public function getH()
    {
        return $this->hScore;
    }
}