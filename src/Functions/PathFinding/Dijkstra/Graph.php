<?php

namespace DataCube\DataCubeAggregation\Functions\PathFinding\Dijkstra;

class Graph
{
    //未访问到的节点
    private $unVasited = [];

    //节点之间的距离，是一个邻接矩阵，保存任意两个节点间的距离
    private $edges;

    //所有节点
    private $nodes;

    //起始节点的下标
    private $startNodeIndex;

    public function __construct(array $nodes, array $edges, int $startNodeIndex)
    {
        $this->nodes = $nodes;
        $this->edges = $edges;
        $this->startNodeIndex = $startNodeIndex;
        $this->initial();
    }

    //标记起点，构造未访问节点数组
    private function initial()
    {
        //在邻接矩阵里查找与起始节点相邻的节点（找路径最短的）
        $arrEdge = $this->edges[$this->startNodeIndex];
        $min = min(array_filter($arrEdge)); //过滤掉路径长度为0的本元素

        $nodeLen = count($arrEdge);

        //构造未访问到的节点
        for ($i = 0; $i < $nodeLen; ++$i) {
            if ($i == $this->startNodeIndex) {
                continue;
            }
            if ($arrEdge[$i] < PHP_INT_MAX) {
                if ($arrEdge[$i] == $min) {//将找到的节点标注已获取最短路径
                    $this->nodes[$i]->isMarked = true;
                    $this->nodes[$i]->minPathDistance = $min;
                } else {
                    $this->nodes[$i]->minPathDistance = $arrEdge[$i];
                    $this->unVisited[$i] = $this->nodes[$i];
                }
            } else {
                $this->unVisited[$i] = $this->nodes[$i];
            }
        }
        $this->stepPrint();
    }

    /**
     * 处理所有未访问的节点，成功后从未访问数组中删除.
     *
     * @return
     */
    public function search()
    {
        //将未访问到的节点遍历一遍，处理完毕后将找到的节点从$unVisited中删除
        while (!empty($this->unVisited)) {
            //查询到与已标记节点相邻的节点，如果有多个相邻的节点，则比较各节点路径长度
            $indexVisited = [];
            foreach ($this->nodes as $k => $v) {
                if ($this->nodes[$k]->isMarked) {
                    $indexVisited[] = $k;
                }
            }

            $minLen = PHP_INT_MAX;
            $minIndex = 0;
            foreach ($this->unVisited as $i => $node) {
                //更新与已标记节点相邻的节点的距离
                $currLen = PHP_INT_MAX;
                foreach ($indexVisited as $j) {
                    if ($this->edges[$i][$j] > 0 && $this->edges[$i][$j] < PHP_INT_MAX) {//相邻
                        // echo "i:{$i},j={$j}, currLen={$currLen}<br/>";
                        if ($currLen > $this->edges[$i][$j] + $this->nodes[$j]->minPathDistance) {
                            $currLen = $this->edges[$i][$j] + $this->nodes[$j]->minPathDistance;
                        }
                    }
                }
                $this->unVisited[$i]->minPathDistance = $currLen;

                //找到路径最短的那条线
                if ($minLen > $node->minPathDistance && $node->minPathDistance > 0) {
                    $minLen = $node->minPathDistance;
                    $minIndex = $i;
                }
            }
            //修改即将删除的节点为已标记
            $this->nodes[$minIndex]->isMarked = true;
            //更新最短线上的节点的路径长度
            $this->nodes[$minIndex]->minPathDistance = $minLen;
            //从未访问节点中删除已查询到的节点
            unset($this->unVisited[$minIndex]);
            $this->stepPrint();
        }
    }

    /**
     * 打印每一步的结果.
     *
     * @return [type] [description]
     */
    private function stepPrint()
    {
        $visited = [];
        foreach ($this->nodes as $k => $v) {
            if ($this->nodes[$k]->isMarked) {
                $visited[] = $v->name;
            }
        }
        $unvisit = [];
        foreach ($this->unVisited as $k => $v) {
            $unvisit[] = $v->name;
        }
        echo "S:".json_encode($visited)."<br/>";
        echo "U:".json_encode($unvisit)."<br/>";
        echo '<br/>';
    }

    /**
     * 打印最终结果.
     *
     * @return [type] [description]
     */
    public function printGraph()
    {
        $len = count($this->nodes);
        for ($i = 0; $i < $len; ++$i) {
            echo "{$this->nodes[$i]->name}({$this->nodes[$i]->minPathDistance}),";
        }
        echo "<br/>";
    }
}
