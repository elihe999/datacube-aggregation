<?php

namespace DataCube\DataCubeAggregation\Functions\PathFinding\Dijkstra;

class Node
{
    //标记该节点是否已得到最短路径
    public $isMarked = false;

    //标记该节点离起始节点的最短路径长度,等于0表示起点
    public $minPathDistance = PHP_INT_MAX;

    //标记节点名称
    public $name;

    public function __construct($name, $distance = null)
    {
        $this->name = $name;
        if (null !== $distance && 0 === $distance) {
            $this->minPathDistance = $distance;
            $this->isMarked = true;
        }
    }
}
