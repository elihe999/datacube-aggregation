<?php
namespace DataCube\DataCubeAggregation\Functions\OLAP;

class PartitionFunc
{
    function bucket($data, $fieldName, $bucketSize, $indexMode = '') {
        // 初始化分区结果数组
        $buckets = array();

        // 遍历数据集中的每个值
        foreach ($data as $row) {
            $value = $row[$fieldName];

            // 计算所属的分区编号
            $bucketIndex = floor($value / $bucketSize);

            // 将值添加到相应的分区中
            if (!isset($buckets[$bucketIndex])) {
                $buckets[$bucketIndex] = array();
            }
            $buckets[$bucketIndex][] = $value;
        }

        return $buckets;
    }
}
