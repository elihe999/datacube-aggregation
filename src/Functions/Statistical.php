<?php

namespace DataCube\DataCubeAggregation\Functions;

use Symfony\Component\OptionsResolver\OptionsResolver;
use DataCube\DataCubeAggregation\Functions\Statistic\StatisticalFunc;

class Statistical
{
    public $validateOptions = null;
    public $data;
    public $size;
    public $result = [
        'anomaly_data' => [],
        'anomaly_rate' => 0,
        'anomaly_num' => 0
    ];
    public function validation(array $data, array $options)
    {
        $resolver = new OptionsResolver();
        $resolver->setDefaults([
            'epslion' => 2,
            'minSamples' => 3
        ]);
        $this->data = $data;
        $this->size = count($data);
        $this->validateOptions = $resolver->resolve($options);
    }

    public function countAvg()
    {}

    public function validateRate()
    {}

    // 分布形态

    // 集中趋势

    // 离散趋势

    // 异常数
    private function validateByThreeSigma()
    {
        [$lower, $upper] = (new StatisticalFunc())->sigmaRange($this->data, 3);
        
        $counter = 0;
        foreach ($this->data as $index => $value) {
            if ($value > $upper || $value < $lower) {
                $this->result['anomaly_data'][] = [$index => $value];
                $counter++;                
            }
        }
        $this->result['anomaly_rate'] = $this->size / $counter;
        $this->result['anomaly_num'] = $counter;
        return $this->result;
    }

    private function zScore()
    {
        $counter = 0;
        $statFunc = new StatisticalFunc();
        foreach ($this->data as $index => $value) {
            $zScore = ($value - $statFunc->arithmetic($this->data) / $statFunc->sampleVariance($this->data));
            if ($zScore > 3) {
                $this->result['anomaly_data'][] = [$index => $value];
                $counter++;   
            }
        }
        $this->result['anomaly_rate'] = $this->size / $counter;
        $this->result['anomaly_num'] = $counter;
        return $this->result;
    }
}
