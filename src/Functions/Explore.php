<?php

namespace DataCube\DataCubeAggregation\Functions;

use Rubix\ML\Datasets\Labeled;
use Rubix\ML\Datasets\Unlabeled;
use Rubix\ML\Datasets\Dataset;
use Symfony\Component\OptionsResolver\OptionsResolver;

class Explore
{
    public Dataset $dataset;

    public function __construct($config)
    {
        $resolver = new OptionsResolver();
        $resolver->setDefined('samples');
        $resolver->setDefined('labels');
        $resolver->setDefined('mode');
        $resolver->setDefined('extractors_rubix');
        $resolver->setAllowedTypes('samples', 'array');
        $resolver->setAllowedTypes('labels', 'array');
        $resolver->setAllowedTypes('mode', ['auto', 'labeled', 'unlabeled', 'custom']);
        $options = $resolver->resolve($config);
        $samples = $options['samples'] ?? null;
        $labels = $options['labels'] ?? null;
        /**
         * the last is labels
         */
        if (!empty($options['extractors_rubix'])) {
            $samples = $labels = [];
            foreach ($options['extractors_rubix'] as $record) {
                $labels[] = array_pop($record);
                $samples[] = $record;
            }
        }
        $this->setSample($samples, $labels);
    }

    function setSample(array $samples, $labels)
    {
        if (!empty($labels)) {
            $this->dataset = new Labeled($samples, $labels);
        } else {
            $this->dataset = new Unlabeled($samples);
        }
    }

    public function getSamples()
    {
        return $this->dataset;
    }

    /**
     * @return mixed
     *
     * toJson toArray
     */
    public function describe()
    {
        return $this->dataset->describe();
    }
}
