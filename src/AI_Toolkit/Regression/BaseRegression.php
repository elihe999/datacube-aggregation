<?php

namespace DataCube\DataCubeAggregation\AI_Toolkit\Regression;

use DataCube\DataCubeAggregation\Functions\MachineLearning\NeuralNetwork\OptimizersTraits;

class BaseRegression
{
    use OptimizersTraits;

    public array $options = [];

    /**
     * @return void
     */
    public function optimizerStrConvertor(): void
    {
        $customOptimizer = $this->options['optimizer'];
        $this->options['optimizer'] = $this->rubixOptimizerArrConvertor($customOptimizer);
    }
}