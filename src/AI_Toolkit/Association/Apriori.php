<?php

namespace DataCube\DataCubeAggregation\AI_Toolkit\Association;

use DataCube\DataCubeAggregation\AI_Toolkit\Interfaces\AssociatingInterface;
use Phpml\Association\Apriori as PhpmlApriori;
use Symfony\Component\OptionsResolver\OptionsResolver;

class Apriori implements AssociatingInterface
{
    public array $options = [];
    public $associator;
    public function __construct(array $options = [])
    {
        // (float $support = 0.0, float $confidence = 0.0)
        $resolver = new OptionsResolver();
        $this->configureOptions($resolver);

        $this->options = $resolver->resolve($options);
        $this->associator = new PhpmlApriori($this->options['support'], $this->options['support']);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'support' => 0.0,
            'confidence' => 0.0,
        ]);
    }

    public function getRules()
    {
        return $this->associator->getRules();
    }

    public function apriori()
    {
        return $this->associator->apriori();
    }
}