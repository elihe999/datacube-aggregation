<?php

namespace DataCube\DataCubeAggregation\AI_Toolkit\Clustering;

use DataCube\DataCubeAggregation\Exception\CustomException;
use DataCube\DataCubeAggregation\Functions\MachineLearning\Clustering\DBSCAN as Phpml_DBSCAN;
use DataCube\DataCubeAggregation\Functions\MachineLearning\Clustering\Rubix_DBSCAN;
use Phpml\Math\Distance\Euclidean;
use Symfony\Component\OptionsResolver\OptionsResolver;

class DBSCAN extends BaseClusterer
{
    use \DataCube\DataCubeAggregation\Traits\MultiML;

    public function __construct(array $options = [])
    {
        $resolver = new OptionsResolver();
        $this->configureOptions($resolver);
        $this->options = $resolver->resolve($options);
        if (!empty($this->options['type'])) {
            if (in_array($this->options['type'], self::mlPackagistNameList())) {
                throw new CustomException('Undefined ML type');
            }
        }
        $internalOption = [];
        switch ($this->options['type']) {
            case 'rubix':
                $this->clusterers = new Rubix_DBSCAN([
                    'radius' => 2,
                    'minDensity' => 3,
                    'tree' => null
                ]);
                break;
            case 'phpml':
            default:
                $this->clusterers = new Phpml_DBSCAN([
                    'epslion' => 2,
                    'minSamples' => 3,
                    'distance' => new Euclidean()
                ]);
        }
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'epslion' => 2,
            'minSamples' => 3
        ]);
    }
}