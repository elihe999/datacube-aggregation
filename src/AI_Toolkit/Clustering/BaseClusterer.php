<?php

namespace DataCube\DataCubeAggregation\AI_Toolkit\Clustering;

class BaseClusterer
{
    public $clusterers = null;
    public array $options = array();
}