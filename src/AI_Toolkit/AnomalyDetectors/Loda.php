<?php

namespace DataCube\DataCubeAggregation\AI_Toolkit\AnomalyDetectors;

use DataCube\DataCubeAggregation\Exception\CustomInvalidArgumentException;
use Rubix\ML\AnomalyDetectors\Loda as RubixLoda;
use Symfony\Component\OptionsResolver\OptionsResolver;

class Loda extends BaseAnomalyDetectors
{
    public function __construct(array $options = [])
    {
        $resolver = new OptionsResolver();
        $this->configureOptions($resolver);

        $this->options = $resolver->resolve($options);
        if ($this->options['contamination'] < 0.0 or $this->options['contamination'] > 0.5) {
            throw new CustomInvalidArgumentException('Contamination must be'
                . " between 0 and 0.5, $this->options['contamination'] given.");
        }

        if ($this->options['estimators'] < 1) {
            throw new CustomInvalidArgumentException('Number of estimators'
                . " must be greater than 0, $this->options['estimators'] given.");
        }

        if (isset($this->options['bins']) and $this->options['bins']< 3) {
            throw new CustomInvalidArgumentException('Bins must be greater'
                . ' than ' . 3 . ", {$this->options['bins']} given.");
        }
        $this->estimator = new RubixLoda(
            $this->options['contamination'],
            $this->options['estimators'],
            $this->options['bins']
        );
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'contamination' => 250,
            'estimators' => 8,
            'bins' => 0.01,
        ]);
    }
}