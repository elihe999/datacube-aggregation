<?php

namespace DataCube\DataCubeAggregation\AI_Toolkit\AnomalyDetectors;

class BaseAnomalyDetectors
{
    public $estimator = null;
    public array $options = array();
}