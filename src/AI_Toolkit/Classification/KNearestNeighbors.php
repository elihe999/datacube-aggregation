<?php

namespace DataCube\DataCubeAggregation\AI_Toolkit\Classification;

use DataCube\DataCubeAggregation\AI_Toolkit\Interfaces\TrainerInterface;
use DataCube\DataCubeAggregation\Exception\CustomException;
use DataCube\DataCubeAggregation\Exception\CustomInvalidArgumentException;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Rubix\ML\Classifiers\KNearestNeighbors as RubixKNearestNeighbors;
use Rubix\ML\Datasets\Labeled;
use Rubix\ML\Datasets\Unlabeled;
use Rubix\ML\Kernels\Distance\Manhattan;

class KNearestNeighbors extends BaseClassifier implements TrainerInterface
{
    public function __construct(array $options = [])
    {
        $resolver = new OptionsResolver();
        $this->configureOptions($resolver);

        $this->options = $resolver->resolve($options);
        $this->classifier = new RubixKNearestNeighbors(
            $this->options['k'],
            $this->options['weighted'],
            $this->options['kernel']
        );
    }

    /**
     *
     * @param OptionsResolver $resolver
     * @return void
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'k' => 10,
            'weighted' => false,
            'kernel' => new Manhattan(),
        ]);
    }

    public function train(array $samples, array $targets): void
    {
        try {
            $this->classifier->train(new Labeled($samples, $targets));
        } catch (\Exception $e) {
        }
    }

    public function predict(array $samples)
    {
        try {
            return $this->classifier->predict(new Unlabeled([$samples]));
        } catch (\Exception $e) {
            throw new CustomException('Could not predict');
        }
    }
}