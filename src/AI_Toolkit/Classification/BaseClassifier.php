<?php

namespace DataCube\DataCubeAggregation\AI_Toolkit\Classification;

use DataCube\DataCubeAggregation\AI_Toolkit\AbstractTrainer;

class BaseClassifier extends AbstractTrainer
{
    public $classifier = null;
    public array $options = array();

}