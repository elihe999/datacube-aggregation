<?php

namespace DataCube\DataCubeAggregation\AI_Toolkit\Classification;

use DataCube\DataCubeAggregation\AI_Toolkit\Interfaces\ProbabilityEstimator;
use DataCube\DataCubeAggregation\AI_Toolkit\Interfaces\TrainerInterface;
use DataCube\DataCubeAggregation\Exception\CustomException;
use DataCube\DataCubeAggregation\Exception\CustomInvalidArgumentException;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Rubix\ML\Classifiers\ClassificationTree as RubixClassificationTree;
use Rubix\ML\Datasets\Labeled;
use Rubix\ML\Datasets\Unlabeled;
/**
 * PHP ML
 */
class ClassificationTree extends BaseClassifier implements TrainerInterface, ProbabilityEstimator
{
    public function __construct(array $options = [])
    {
        $resolver = new OptionsResolver();
        $this->configureOptions($resolver);
        $this->options = $resolver->resolve($options);
        $this->classifier = new RubixClassificationTree(
            $this->options['maxHeight'],
            $this->options['maxLeafSize'],
            $this->options['minPurityIncrease'],
            $this->options['maxFeatures'],
            $this->options['maxBins'],
        );
    }

    /**
     *  int $maxHeight = PHP_INT_MAX,
     *  int $maxLeafSize = 3,
     *  float $minPurityIncrease = 1e-7,
     *  ?int $maxFeatures = null,
     *  ?int $maxBins = null
     *
     * @param OptionsResolver $resolver
     * @return void
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'maxHeight' => PHP_INT_MAX,
            'maxLeafSize' => 3,
            'minPurityIncrease' => 1e-7,
            'maxFeatures' => null,
            'maxBins' => null,
        ]);
    }

    public function train(array $samples, array $targets): void
    {
        try {
            $this->classifier->train(new Labeled($samples, $targets));
        } catch (\Exception $e) {
        }
    }

    public function predict(array $samples)
    {
        try {
            return $this->classifier->predict(new Unlabeled([$samples]));
        } catch (\Exception $e) {
            throw new CustomException('Could not predict');
        }
    }

    public function predictProbability(array $sample)
    {
        if ($this->options['probabilityEstimates']) {
            try {
                return $this->classifier->predictProbability($sample);
            } catch (\Exception $e) {
            }
        }
        throw new \InvalidArgumentException('To predict probabilities you must build a classifier with $probabilityEstimates set to true.');
    }
}