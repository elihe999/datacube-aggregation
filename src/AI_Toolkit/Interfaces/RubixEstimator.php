<?php
namespace DataCube\DataCubeAggregation\AI_Toolkit\Interfaces;

interface RubixEstimator
{
    public function predict($target);
}