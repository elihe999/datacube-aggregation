<?php

namespace DataCube\DataCubeAggregation\AI_Toolkit\Interfaces;
interface TrainerInterface
{
    public function train(array $samples, array $targets): void;

    /**
     * @return mixed
     */
    public function predict(array $samples);
}