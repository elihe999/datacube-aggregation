<?php
namespace DataCube\DataCubeAggregation\AI_Toolkit\Interfaces;

interface ProbabilityEstimator
{
    public function predictProbability(array $sample);
}