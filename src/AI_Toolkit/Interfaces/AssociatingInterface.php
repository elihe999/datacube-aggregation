<?php

namespace DataCube\DataCubeAggregation\AI_Toolkit\Interfaces;

interface AssociatingInterface
{
    public function getRules();
    public function apriori();
}