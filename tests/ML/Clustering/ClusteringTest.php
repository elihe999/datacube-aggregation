<?php declare(strict_types=1);

namespace ML\Clustering;

use PHPUnit\Framework\TestCase;
use Rubix\ML\CrossValidation\Metrics\Homogeneity;
use Rubix\ML\CrossValidation\Reports\ContingencyTable;
use Rubix\ML\Datasets\Labeled;
use Rubix\ML\CrossValidation\Metrics\Accuracy;
use Rubix\ML\Datasets\Generators\Agglomerate;
use Rubix\ML\Datasets\Generators\Blob;

final class ClusteringTest extends TestCase
{
    public $training;
    public $testing;
    public function __construct(?string $name = null, array $data = [], $dataName = '')
    {
        $generator = new Agglomerate([
            'red' => new Blob([255, 0, 0], 20.0),
            'orange' => new Blob([255, 128, 0], 10.0),
            'yellow' => new Blob([255, 255, 0], 10.0),
            'green' => new Blob([0, 128, 0], 20.0),
            'blue' => new Blob([0, 0, 255], 20.0),
            'aqua' => new Blob([0, 255, 255], 10.0),
            'purple' => new Blob([128, 0, 255], 10.0),
            'pink' => new Blob([255, 0, 255], 10.0),
            'magenta' => new Blob([255, 0, 128], 10.0),
            'black' => new Blob([0, 0, 0], 10.0),
        ]);
        [$this->training, $this->testing] = $generator->generate(5000)->stratifiedSplit(0.8);

        parent::__construct($name, $data, $dataName);
    }

    public function testGMMPredict(): void
    {
        $estimator = new \DataCube\DataCubeAggregation\Functions\MachineLearning\Clustering\GaussianMixture([
            'estimator' => 10
        ]);

        $estimator->train($this->training);
        $predictions = $estimator->predict($this->testing);
        $report = new ContingencyTable();

        $results = $report->generate($predictions, $this->testing->labels());

        echo $results;
        $metric = new Homogeneity();

        $score = $metric->score($predictions, $this->testing->labels());
        var_dump($results);
        $this->assertEquals(count($results), 10);
        $this->assertGreaterThan(0.8, $score);
    }
}