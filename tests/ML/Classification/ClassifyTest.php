<?php declare(strict_types=1);

namespace ML\Classification;

use PHPUnit\Framework\TestCase;

final class ClassifyTest extends TestCase
{
    public function testSVCPredict(): void
    {

        $samples = [[1, 3], [1, 4], [2, 4], [3, 1], [4, 1], [4, 2]];
        $labels = ['a', 'a', 'a', 'b', 'b', 'b'];

        $classifier = new \DataCube\DataCubeAggregation\AI_Toolkit\Classification\SVC([
            'kernel' => 1,
            'cost' => 1000
        ]);
        $classifier->train($samples, $labels);
        $this->assertEquals('b', $classifier->predict([3, 2]));
    }

    public function testAdaBoostPredict():void
    {
        $samples = [[1, 3], [1, 4], [2, 4], [3, 1], [4, 1], [4, 2]];
        $labels = ['a', 'a', 'a', 'b', 'b', 'b'];
        $classifier = new \DataCube\DataCubeAggregation\AI_Toolkit\Classification\AdaBoost();
        $classifier->train($samples, $labels);
        $this->assertEquals(['b'], $classifier->predict([3, 2]));
    }

    public function testClassificationTreePredict():void
    {
        $samples = [[1, 3], [1, 4], [2, 4], [3, 1], [4, 1], [4, 2]];
        $labels = ['a', 'a', 'a', 'b', 'b', 'b'];
        $classifier = new \DataCube\DataCubeAggregation\AI_Toolkit\Classification\ClassificationTree();
        $classifier->train($samples, $labels);
        $this->assertEquals(['b'], $classifier->predict([3, 2]));
    }
}