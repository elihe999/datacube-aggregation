<?php declare(strict_types=1);

namespace ML\Regression;

use PHPUnit\Framework\TestCase;
use Rubix\ML\NeuralNet\Optimizers\Adam;
use Rubix\ML\NeuralNet\CostFunctions\HuberLoss;

final class AdalineTest extends TestCase
{
    public function testPredict(): void
    {

        $xs = [[-2.0], [-1.0], [0.0], [1.0], [2.0], [3.0], [4.0]];
        $ys = [-3.0, -1.0, 1.0, 3.0, 5.0, 7.0, 9.0];

        $estimator = new \DataCube\DataCubeAggregation\AI_Toolkit\Regression\Adaline([
            'batchSize' => 256,
            'optimizer' => new Adam(0.001),
            'l2Penalty' => 1e-4,
            'epochs' => 500,
            'minChange' => 1e-6,
            'window' => 5,
            'costFn' => new HuberLoss(2.5)
        ]);
        $estimator->train($xs, $ys);
        $this->assertIsFloat($estimator->predict(0.5)[0]);
    }

    public function testStringParam(): void
    {

        $xs = [[-2.0], [-1.0], [0.0], [1.0], [2.0], [3.0], [4.0]];
        $ys = [-3.0, -1.0, 1.0, 3.0, 5.0, 7.0, 9.0];

        $estimator = new \DataCube\DataCubeAggregation\AI_Toolkit\Regression\Adaline([
            'batchSize' => 256,
            'optimizer' => ['Adam', 0.001],
            'l2Penalty' => 1e-4,
            'epochs' => 500,
            'minChange' => 1e-6,
            'window' => 5,
            'costFn' => new HuberLoss(2.5)
        ]);
        $estimator->train($xs, $ys);
        print_r($estimator->predict(0.5)[0]);
        $this->assertIsFloat($estimator->predict(0.5)[0]);
    }

//    public function testWrongParam()
//    {
//        try {
//            $estimator = new \DataCube\DataCubeAggregation\AI_Toolkit\Regression\Adaline([
//                'batchSize' => 256,
//                'optimizer' => 'wrong_param',
//            ]);
//        } catch (\Exception $e) {
//            $this->assertTrue($e->get instanceof \TypeError);
//        }
//    }
}