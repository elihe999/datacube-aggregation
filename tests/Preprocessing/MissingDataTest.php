<?php declare(strict_types=1);
use PHPUnit\Framework\TestCase;

final class MissingDataTest extends TestCase
{
    public function testCanCallByMainFile(): void
    {
        $a = [
            [1,2,3,null],
            [1,2,3,4],
            [1,null,3,4],
            [null,2,3,4],
        ];
        $expected = [
            [1,2,3,0],
            [1,2,3,4],
            [1,0,3,4],
            [0,2,3,4],
        ];
        $actual = (new \DataCube\DataCubeAggregation\Functions\Cleaning())->MissingData()->replaceWith($a, [null], 0);
        $this->assertEquals($expected, $actual);
    }
}
