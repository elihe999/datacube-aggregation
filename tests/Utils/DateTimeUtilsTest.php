<?php declare(strict_types=1);
use PHPUnit\Framework\TestCase;

use DataCube\DataCubeAggregation\Utils\DateTime;
use DataCube\DataCubeAggregation\Utils\DateTime\GroupByFunc;

final class DateTimeUtilsTest extends TestCase
{
    public function testCallFunction(): void
    {
        $dataset = [
            1697093269,
            1697093323,
            1673456800,
            1673452800,
            1673452500,
        ];
        $origin = (new GroupByFunc())->groupTimestampsByMonth($dataset);
        $called = (new DateTime())->groupBy('groupTimestampsByMonth', $dataset);
        $this->assertEquals($origin, $called);
        $rename = (new DateTime())->groupTimestampsByMonth($dataset);
        $this->assertEquals($origin, $rename);
    }

    public function testStaticCall(): void
    {
        $this->assertEquals(true, DateTime::isTimestamp(1697093269));
    }
}