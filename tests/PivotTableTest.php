<?php declare(strict_types=1);
use PHPUnit\Framework\TestCase;
use DataCube\DataCubeAggregation\Utils\Graphics\Pivot;

final class PivotTableTest extends TestCase
{
    public function testTableShow(): void
    {
        $this->expectException(Exception::class);
        throw new Exception();
    }

    public function testArrayGroupBy()
    {
        $records = [
            [
                'state'  => 'IN',
                'city'   => 'Indianapolis',
                'object' => 'School bus',
            ],
            [
                'state'  => 'IN',
                'city'   => 'Indianapolis',
                'object' => 'Manhole',
            ],
            [
                'state'  => 'IN',
                'city'   => 'Plainfield',
                'object' => 'Basketball',
            ],
            [
                'state'  => 'CA',
                'city'   => 'San Diego',
                'object' => 'Light bulb',
            ],
            [
                'state'  => 'CA',
                'city'   => 'Mountain View',
                'object' => 'Space pen',
            ],
        ];
        
        $grouped = (new Pivot)->array_group_by($records, 'state', 'city');
        $expected = [
            'IN' => [
                'Indianapolis' => [
                    ['state' => 'IN', 'city' => 'Indianapolis', 'object' => 'School bus'],
                    ['state' => 'IN', 'city' => 'Indianapolis', 'object' => 'Manhole'],
                ],
                'Plainfield' => [
                    ['state' => 'IN', 'city' => 'Plainfield', 'object' => 'Basketball'],
                ]
            ],
            'CA' => [
                'San Diego' => [
                    ['state' => 'CA', 'city' => 'San Diego', 'object' => 'Light bulb'],
                ],
                'Mountain View' => [
                    [
                        'state'  => 'CA',
                        'city'   => 'Mountain View',
                        'object' => 'Space pen',
                    ]
                ]
            ]
        ];
        $this->assertEquals($expected, $grouped);
    }
}