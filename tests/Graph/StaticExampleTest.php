<?php

namespace Graph;

use DataCube\DataCubeAggregation\Functions\PathFinding\AStar\Terrain\Position;
use DataCube\DataCubeAggregation\Functions\PathFinding\AStar\Terrain\StaticExample;
use DataCube\DataCubeAggregation\Functions\PathFinding\AStar\Terrain\TerrainCost;
use PHPUnit\Framework\TestCase;

class StaticExampleTest extends TestCase
{
    public function testShouldPrintSolution(): void
    {
        $terrainCost = new TerrainCost([
            [3, 2, 3, 6, 1],
            [1, 3, 4, 1, 1],
            [3, 1, 1, 4, 1],
            [1, 1, 5, 2, 1],
        ]);

        $start = new Position(0, 0);
        $goal = new Position(0, 4);

        $expectedOutput = <<<HEREDOC
  1  -  -  -  6
  2  -  -  5  -
  -  3  4  -  -
  -  -  -  -  -
HEREDOC;

        StaticExample::printSolution($terrainCost, $start, $goal);

        $this->expectOutputString($expectedOutput);
    }
}
