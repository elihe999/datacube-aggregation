<?php

declare(strict_types=1);

use PHPUnit\Framework\TestCase;
use DataCube\DataCubeAggregation\Functions\Statistic\StatisticalGraphics;

final class StaticGraphTest extends TestCase
{
    public function testStaticGraph(): void
    {
        $sep = "\t";
        $nl  = "\n";

        $content = file_get_contents(__DIR__ . '/../../src/Data/car.csv');

        $records = explode($nl, $content);
        $header  = explode($sep, trim(array_shift($records)));
        $data    = array_fill_keys($header, array());

        foreach ($records as $id => $record) {
            $record = trim($record);
            if ($record == '') continue;

            $fields = explode($sep, $record);
            $titles = $header;

            foreach ($fields as $field) {
                $title = array_shift($titles);
                $data[$title][] = $field;
            }
        }

        $x = $data['wt'];
        $y = $data['mpg'];
        echo 'Boxplot: <br /><pre>';
        $graph = (new StatisticalGraphics());
        print_r($graph->boxplot($x));
        echo '</pre><br />';

        echo 'Histogram: <br /><pre>';
        print_r($graph->hist($x, 8));
        echo '</pre><br />';

        echo 'Normal Q-Q Plot: <br />';
        $qq = $graph->qqnorm($x);
        echo 'x = ' . implode(', ', $qq['x']) . '<br />';
        echo 'y = ' . implode(', ', $qq['y']) . '<br />';

        echo '<hr />';
    }
}
