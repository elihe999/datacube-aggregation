<?php

namespace Graph;

use DataCube\DataCubeAggregation\Functions\PathFinding\Dijkstra\Graph;
use DataCube\DataCubeAggregation\Functions\PathFinding\Dijkstra\Node;
use PHPUnit\Framework\TestCase;

class DijkstraTest extends TestCase
{
    public function testFindPaths()
    {
        //构造节点数组
        $nodes = [];
        $node = new Node('A');
        $nodes[] = $node;
        $node = new Node('B');
        $nodes[] = $node;
        $node = new Node('C');
        $nodes[] = $node;
        $node = new Node('D', 0); //D节点是起始节点
        $nodes[] = $node;
        $startNodeIndex = count($nodes) - 1;   //起始节点的下标
        $node = new Node('E');
        $nodes[] = $node;
        $node = new Node('F');
        $nodes[] = $node;
        $node = new Node('G');
        $nodes[] = $node;

        //节点之间是否有弧边，弧边的长度,0-6分别对应nodes中的下标
        $max = PHP_INT_MAX;
        //节点A      A     B     C     D     E     F    G
        $edges[0] = [0, 12, $max, $max, $max, 16, 14];  //A
        $edges[1] = [12, 0, 10, $max, $max, 7, $max];   //B
        $edges[2] = [$max, 10, 0, 3, 5, 6, $max];       //C
        $edges[3] = [$max, $max, 3, 0, 4, $max, $max];   //D
        $edges[4] = [$max, $max, 5, 4, 0, 2, 8];
        $edges[5] = [16, 7, 6, $max, 2, 0, 8];
        $edges[6] = [14, $max, $max, $max, 8, 9, 0];

        $graph = new Graph($nodes, $edges, $startNodeIndex);
        $graph->search();
        $graph->printGraph();
        $expectedOutput = 'S:["C","D"]<br/>U:["A","B","E","F","G"]<br/><br/>S:["C","D","E"]<br/>U:["A","B","F","G"]<br/><br/>S:["C","D","E","F"]<br/>U:["A","B","G"]<br/><br/>S:["C","D","E","F","G"]<br/>U:["A","B"]<br/><br/>S:["B","C","D","E","F","G"]<br/>U:["A"]<br/><br/>S:["A","B","C","D","E","F","G"]<br/>U:[]<br/><br/>A(22),B(13),C(3),D(0),E(4),F(6),G(12),<br/>';
        $this->expectOutputString($expectedOutput);
    }
}
