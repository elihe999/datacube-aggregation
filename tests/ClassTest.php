<?php declare(strict_types=1);
use PHPUnit\Framework\TestCase;

final class ClassTest extends TestCase
{
    public function testCanThrow(): void
    {
        $this->expectException(Exception::class);
        throw new Exception();
    }
}