<?php
require_once __DIR__ . '/../vendor/autoload.php';

// use Phpml\Classification\KNearestNeighbors;

// $samples = [[1, 3], [1, 4], [2, 4], [3, 1], [4, 1], [4, 2]];
// $labels = ['a', 'a', 'a', 'b', 'b', 'b'];

// $classifier = new KNearestNeighbors();
// $classifier->train($samples, $labels);

// var_dump($classifier->predict([3, 2]));

// use DataCube\DataCubeAggregation\Functions\MachineLearning\Clustering\KMeans;

// $samples = [[1, 3], [1, 4], [2, 4], [3, 1], [4, 2]];

// $values = array(1,2,3,4,5);
// $cluster_map = array(
//     0 => 2,
//     1 => 4,
//     2 => 3,
//     3 => 0,
//     4 => 1,
// );

// $clusters = (new KMeans())->kmeans_populate_clusters($values, $cluster_map);
// var_dump($clusters);

// $input = array(1, 3, 2, 5, 6, 2, 3, 1, 30, 36, 45, 3, 15, 17);
// $k =  3;
// $clusters = (new KMeans())->kMeans($input, $k);
// var_dump($clusters);

// $input = array(
//     array('age' => 1),
//     array('age' => 3),
//     array('age' => 2),
//     array('age' => 5),
//     array('age' => 6),
//     array('age' => 2),
//     array('age' => 3),
//     array('age' => 1),
//     array('age' => 30),
//     array('age' => 36),
//     array('age' => 45),
//     array('age' => 3),
//     array('age' => 15),
//     array('age' => 17),
// );
// $k = 3;
// $clusters = (new Kmeans())->kmeans($input, $k, 'age');
// var_dump($clusters);

// use DataCube\DataCubeAggregation\Utils\DateTime;
// use DataCube\DataCubeAggregation\Utils\DateTime\GroupByFunc;
// $dataset = [
//     1697093269,
//     1697093323,
//     1673456800,
//     1673452800,
//     1673452500,
// ];
// $origin = (new GroupByFunc())->groupTimestampsByMonth($dataset);
// $rename = (new DateTime())->groupTimestampsByMonth($dataset);
// $called = (new DateTime())->groupBy('groupTimestampsByMonth', $dataset);
// var_dump($rename);

// use Rubix\ML\Extractors\CSV;

// $actual = (new \DataCube\DataCubeAggregation\Functions\Explore([
//     'extractors_rubix' => new CSV(__DIR__ . '/../src/Data/housing.csv', false)
// ]))->describe()->toArray();
// print_r(json_encode($actual));
// $samples = [];
// foreach (file(__DIR__ . '/../src/Data/housing.csv') as $line) {
//     $samples[] = explode(',', trim($line));
// }
// $actual = (new \DataCube\DataCubeAggregation\Functions\Explore([
//     'samples' => $samples
// ]))->describe()->toArray();
$samples = [[1, 3], [1, 4], [2, 4], [3, 1], [4, 1], [4, 2]];
        $labels = ['a', 'a', 'a', 'b', 'b', 'b'];
        $classifier = new \DataCube\DataCubeAggregation\AI_Toolkit\Classification\ClassificationTree();
        $classifier->train($samples, $labels);
        print_r($classifier->predict([3, 2]));